import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

import 'DisplayPictureScreen.dart';

class Preview extends StatefulWidget {
  const Preview({super.key, required this.camera});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final CameraDescription camera;

  @override
  State<Preview> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Preview> {
  late CameraController _cameraController;
  late Future<void> _initializeControllerFuture;
  XFile? _image;
  @override
  void initState() {
    super.initState();
    // To display the current output from the Camera,
    // create a CameraController.
    _cameraController = CameraController(
      // Get a specific camera from the list of available cameras.
      widget.camera,
      // Define the resolution to use.
      ResolutionPreset.medium,
    );

    // Next, initialize the controller. This returns a Future.
    _initializeControllerFuture = _cameraController.initialize();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _cameraController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        body: FutureBuilder<void>(
          future: _initializeControllerFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              // Quand l'initialisation de la caméra est terminé,
              // afficher l'aperçu
              return CameraPreview(_cameraController);
            } else {
              // Otherwise, display a loading indicator.
              return const Center(child: CircularProgressIndicator());
            }
          },
        ),
        bottomNavigationBar: BottomNavigationBar(
            items: const [
              BottomNavigationBarItem(
                icon: Icon(Icons.photo),
                label: 'Voir Photo',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.photo_camera),
                label: 'Prendre photo',
              ),
            ],
            onTap: (int index) async {
              switch(index) {
                case 0:
                  await Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => DisplayPictureScreen(
                        // Pass the automatically generated path to
                        // the DisplayPictureScreen widget.
                        imagePath: _image!.path,
                      ),
                    ),
                  );
                  break;
                case 1:
                  await _initializeControllerFuture;
                  // Prendre la photo
                  // et sauvegarder l'endroit où elle a été stockée
                  _image = await _cameraController.takePicture();
                  break;
              }
            }
        )
    );
  }
}
