import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

import 'screens/preview.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Liste des caméras disponible sur l'appareil
  final cameras = await availableCameras();

  // Pour l'exercice, on prend la première caméra de la liste
  // L'objet CameraDescription contient les informations sur la caméra
  // Ex: lensDirection (selfie, caméra arrière), focale, etc
  CameraDescription rearCamera = cameras.first;

  runApp(MaterialApp(
    theme: ThemeData.dark(),
    home: Preview(
      // Pass the appropriate camera to the TakePictureScreen widget.
      camera: rearCamera,
    ),
  ),
  );
}

